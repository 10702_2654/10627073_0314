package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button bt4;
    public Button bt1;
    public Button bt2;
    public Button bt3;
    public Button bt5;
    public Button bt6;
    public Button bt7;
    public Button bt8;
    public Button bt9;
    public Button bt0;
    public Label monitor;
    public int a = 0, b = 0;
    public String e="";

    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                e = "+";
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case ".":
                e = ".";
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "-":
                e = "-";
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "*":
                e = "*";
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "/":
                e = "/";
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "=":
                b = Integer.parseInt(monitor.getText());
                switch (e) {
                    case "+":
                        monitor.setText(String.valueOf(a + b));
                        break;
                    case "-":
                        monitor.setText(String.valueOf(a - b));
                        break;
                    case "*":
                        monitor.setText(String.valueOf(a * b));
                        break;
                    case "/":
                        monitor.setText(String.valueOf(a / b));
                        break;
                }
        }
    }
}

